//
// Created by max on 13.11.20.
//

#ifndef GENERIC_BOARD2_SPI_H
#define GENERIC_BOARD2_SPI_H
#define SPI_PORT PORTB
#define SPI_PORT_SS PORTA
#define SPI_DDR DDRB
#define SPI_DDR_SS DDRA
#define SPI_PIN PINB
#define SPI_MISO (1 << PB4)
#define SPI_SCLK (1 << PB6)
#define SPI_SSB (1 << PA6)
#define SPI_MOSI (1 << PB3)

extern void spiSetup();
extern void spiWrite(uint8_t addr, uint8_t data);
extern void spiWriteMult(uint8_t addr, uint8_t length, uint8_t* values);
extern uint8_t spiRead(uint8_t addr);
extern void spiWriteRead(uint8_t *data);
extern void spiReadMult(uint8_t addr, uint8_t length, uint8_t *values) ;
#endif //GENERIC_BOARD2_SPI_H

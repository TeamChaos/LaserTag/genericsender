//
// Created by max on 13.11.20.
//

#include <avr/io.h>
#include "SPI.h"
#include "../CommonLib/avr_libs/timers.h"


void spiSetup(){
    SPI_DDR |= SPI_SCLK | SPI_MOSI;
    SPI_DDR_SS |= SPI_SSB;
    SPI_DDR &= ~(SPI_MISO);
    SPI_PORT &= ~(SPI_SCLK | SPI_MOSI);
    SPI_PORT_SS |=  SPI_SSB;


}

void spiWriteRead(uint8_t *data) {
    //Mode0
    for (uint8_t bit = 0x80; bit; bit >>= 1) {
        if (*data & bit) SPI_PORT |= SPI_MOSI;
        else
            SPI_PORT &= ~SPI_MOSI;
        SPI_PORT |= SPI_SCLK;

        if (SPI_PIN & SPI_MISO) {
            *data |= bit;
        } else {
            *data &= ~(bit);
        }
        SPI_PORT &= ~SPI_SCLK;

    }
}





void spiWrite(uint8_t addr, uint8_t data) {
    SPI_PORT_SS &= ~(SPI_SSB);
    delay_usec(1);
    uint8_t v[2];
    v[0]=addr;
    v[1]=data;
    spiWriteRead(v);
    spiWriteRead(v+1);


    delay_usec(1);
    SPI_PORT_SS |= SPI_SSB;
}

void spiWriteMult(uint8_t addr, uint8_t length, uint8_t *values) {
    SPI_PORT_SS &= ~(SPI_SSB);
    delay_usec(1);
    uint8_t data[1];
    data[0]=addr;
    spiWriteRead(data);
    for (uint8_t i = 0; i < length; i++) {
        spiWriteRead(values + i);
    }
    delay_usec(1);
    SPI_PORT_SS |= SPI_SSB;

}

uint8_t spiRead(uint8_t addr) {
    SPI_PORT_SS &= ~(SPI_SSB);
    delay_usec(1);
    uint8_t v[2];
    v[0]= addr;
    v[1]=0;
    spiWriteRead(v);
    spiWriteRead(v+1);

    delay_usec(1);
    SPI_PORT_SS |= SPI_SSB;
    return v[1];
}

void spiReadMult(uint8_t addr, uint8_t length, uint8_t *values) {
    SPI_PORT_SS &= ~(SPI_SSB);
    delay_usec(1);
    uint8_t v[1];
    v[0]= addr;
    spiWriteRead(v);
    for (uint8_t i = 0; i < length; i++) {
        spiWriteRead(values + i);
    }


    delay_usec(1);
    SPI_PORT_SS |= SPI_SSB;
}


#define F_CPU  8000000UL
//#define DEBUG

#include <version.h> //Generated git version file

#include "lib-mfrc522/MFRC522.h"

#ifdef __cplusplus
extern "C"
{
#endif
#include "CommonLib/avr_libs/timers.h"
#include "CommonLib/ir/send.h"
#include "CommonLib/avr_libs/TWI_slave.h"
#include "CommonLib/weapon_control/protocol.h"
#include "CommonLib/signals.h"
#ifdef __cplusplus
}
#endif

#define FUSE_DELAY 500
#define DETONATION_DELAY 5000
#define PRE_DENOTATION_LED_DURATION 1150
#define ALIVE_SIGNAL_DELAY 600000
#define ALIVE_SIGNAL_INTERVAL 30000


const char gitversion[8] = GIT_COMMIT_HASH;




MFRC522 rfid;
uint32_t last_seen =0;
uint32_t activated =0;
uint32_t last_alive_signal = 0;
uint8_t last_id = 0xFF;
uint32_t exploded =0;





//################### Main Code ###################### //

#define LED_ON PORTA |= (1 << PA2);
#define LED_OFF PORTA &= ~(1<< PA2);
#define SOUND_ON  PORTA |= (1 << PA4);
#define SOUND_OFF PORTA &= ~(1<<PA4);

bool handleRFID() {
    if (!rfid.PICC_IsNewCardPresent()) return false;
    if (!rfid.PICC_ReadCardSerial()) return false;
    rfid.PCD_StopCrypto1();

    if(rfid.uid.uidByte[0]==0x59&&rfid.uid.uidByte[1]==0x80&&rfid.uid.uidByte[2]==0xEE && rfid.uid.uidByte[3]==0xb8) {
        last_id = 0;
    }
    else{
        return false;
    }
    return true;
}

void explode(){
    SOUND_ON
    LED_ON
    delay(100);
    SOUND_OFF
    LED_OFF
    delay(300);
    SOUND_ON
    LED_ON
    delay(100);
    SOUND_OFF
    LED_OFF
    delay(200);
    SOUND_ON
    LED_ON
    delay(100);
    SOUND_OFF
    LED_OFF
    delay(100);
    SOUND_ON
    LED_ON
    delay(100);
    SOUND_OFF
    LED_OFF
    delay(50);
    SOUND_ON
    LED_ON
    delay(70);
    SOUND_OFF
    LED_OFF
    delay(30);



    SOUND_ON
    LED_ON
    sendIRMessage(false, SIGNAL_ID_GRENADE, last_id);
    delay(800);
    SOUND_OFF
    LED_OFF
    exploded = millis();
}


void loop() {


    delay(10);

    if(handleRFID()){
        if(activated){
            activated = 0;
        }
        last_seen = millis();
        if(exploded){
            exploded = 0;
            LED_ON
            delay(400);
            LED_OFF
            delay(100);
            LED_ON
            delay(100);
            LED_OFF
        }
    }
    else{
        if(!activated && last_id != 0xFF){
            if(millis()-last_seen> FUSE_DELAY ){
                activated = millis();
                LED_ON
                SOUND_ON
                delay(25);
                LED_OFF
                delay(50);
                LED_ON
                delay(100);
                LED_OFF
                SOUND_OFF


            }
        }
    }
    if(activated&&exploded==0){
        if(millis()-activated> (DETONATION_DELAY - PRE_DENOTATION_LED_DURATION) ){
            explode();
        }
    }
    if(exploded != 0 && (millis() - exploded) >  ALIVE_SIGNAL_DELAY && (millis() - last_alive_signal) > ALIVE_SIGNAL_INTERVAL){
        LED_ON
        SOUND_ON
        delay(250);
        SOUND_OFF
        delay(100);
        SOUND_ON
        delay(250);
        LED_OFF
        SOUND_OFF
        last_alive_signal = millis();
    }


}





void setup() {
    initTimers();
    rfid.PCD_Init();



    DDRA |= (1 << PA2); //Status LED as output
    DDRA |= (1 <<PA4); //Sound as output



    LED_ON
    delay(500);
    LED_OFF
    delay(500);
    LED_ON
    delay(500);
    LED_OFF
    delay(500);

}

int main(void) {
    setup();



    while (1) {
        loop();
    }
    return 0;
}



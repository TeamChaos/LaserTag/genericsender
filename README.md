# Generic Sender Control Code

This code controlls the AVR microcontroller on the generic sender PCB.

The exact microcontroller used here is the MicroChip Attiny 861A.
A 261/461 would be fine as well. They have 2K/4K Flash and 128/256 Bytes of SRAM respectively.

[Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/doc8197.pdf)

## AVR Configuration
The microcontroller has to run with full 8 MHz without clock divider. It is also recommend to enable brown-out detection (for 1.8V).
Recommended fuse settings:  
``` -U lfuse:w:0xe2:m -U hfuse:w:0xde:m -U efuse:w:0xff:m  ```

If you use a different device or frequency you will have to adjust the CMakeList.txt:
```
SET(DEVICE "attiny861")
SET(FREQ "8000000")
add_definitions(-DATTINY861)
```

## Function
This reads the configuration from the PA0-PA5 pins (pull-up input) and transmits an IR code frequently based on that configuration.


## Development Setup
To work on this project you need the several packages:  
avr-binutils, avr-gcc, and avr-libc  
Don't forget about git of course.  

A CMake compatible IDE is recommended.  
E.g. CLion or KDevelop (free)  
QtCreator is supposed to work as well

